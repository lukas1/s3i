from s3i import IdentityProvider, Directory, Broker, ServiceReply, TokenType
import uuid
import json
import base64
import time
import os


def callback(ch, method, properties, body):
    """
    This function will be called and executed every time that S3I Broker delivers messages to the consuming application (DT). \n
    It passes a request to service function getProductionDataService() and sends a service reply to the requester.

    Args:
        ch: channel used during consuming, which binds productor and consumer \n
        method: „method“ contains meta information regarding message delivery: It provides delivery flags, redelivery flags, and a routing key that can be used to acknowledge the message. \n
        properties: properties of queue and exchange which are used during consuming \n
        body: content of message  
    """
    body_str = body.decode('utf8').replace("'", '"')
    body_json = json.loads(body_str)
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                   ) + "]: Forest machine has received a new service request")
    '''
    call service function
	'''
    results = getProductionDataService(body_json["serviceType"])

    '''
    prepare service reply 
    '''
    sender = "s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63"
    msg_uuid = "s3ib:" + str(uuid.uuid4())
    receivers = list()
    receiver_endpoints = list()
    receivers.append(body_json["sender"])
    receiver_endpoints.append(body_json["replyToEndpoint"])
    service_type = body_json["serviceType"]
    replyingToUUID = body_json["identifier"]

    servReply = ServiceReply()
    servReply.fillServiceReply(senderUUID=sender, receiverUUID=receivers, serviceType=service_type,
                               results=results, msgUUID=msg_uuid, replyingToUUID=replyingToUUID)
    '''
    send service reply to the requester
    '''
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                   ) + "]: Forest machine sends the service reply")
    # demo_harvester.send_service_reply(sender, receivers, receiver_endpoints,
    #                                  service_type, results, msg_uuid, replyingToUUID)
    demo_harvester.send(receiver_endpoints, servReply.msg.__str__())


def getProductionDataService(service_type):
    """
    Simulates a get production data service and returns a BASE64-encoded StanForD 2010 HPR production data file

    Args:
        service_type(str): service type
    """

    if service_type == "fml40.Harvester.getProductionData":
        print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                       ) + "]: Forest machine collects production data...")
        time.sleep(3)  # simulate the service action

        '''
        for this demo, a dummy demo3.hpr has already been prepared for the service reply. 
        '''
        cwd = os.path.dirname(__file__)
        path = os.path.join(cwd, "demo_send_data", "demo3.hpr")

        '''
        convert the data to BASE64
        '''

        with open(path, "rb") as f:
            base64_data = base64.b64encode(f.read())
            data = base64_data.decode()

        '''
        store the production data in a dict
        '''
        result = dict()
        result["productionData"] = data
        return result
    else:
        return None


if __name__ == "__main__":

    """
    Main function for demo 3: simulate a DT forest machine with a service providing production data. Start a listener and wait for a service request.

    """

    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S",
                                   time.localtime()) + "]: DEMO 3, DT harvester is started.")

    '''
    self authentication at S3I Identity Provider with client secret (without username and password)
    with the grant type "client_credentials"
    '''

    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) +
          "]: Self authentication with known client credentials at S3I Identity Provider.")
    keycloak = IdentityProvider(grant_type='client_credentials', identity_provider_url="https://idp.s3i.vswf.dev/",
                                realm='KWH', client_id="s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63", client_secret="950dc82f-1bac-4852-a3ee-1e62b22e258e")
    access_token = keycloak.get_token(TokenType.ACCESS_TOKEN)
    time.sleep(2)
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S",
                                   time.localtime()) + "]: Token received.")

    '''
    authentication with access token at S3I Directory 
    '''

    s3i_directory = Directory(
        s3i_dir_url="https://dir.s3i.vswf.dev/api/2/", token=access_token)

    '''
    id and endpoint of harvester are known 
    '''
    harvester_id = "s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63"
    harvester_service_endpoint = "s3ib://s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63"

    '''
    authentication with access token (JWT)  at S3I Broker
    '''
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                   ) + "]: Authentication with access token (JWT) at S3I Broker.")

    demo_harvester = Broker(
        auth_form='Username/Password', username=" ", password=access_token, host="rabbitmq.s3i.vswf.dev")

    '''
    start the receive function and wait for a service request
    '''
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) +
          "]: Callback function is started. Now waiting for a service request.")

    demo_harvester.receive(queue=harvester_service_endpoint, callback=callback)
