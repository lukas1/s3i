from s3i import IdentityProvider, Directory, Broker, UserMessage, TokenType
import uuid
import json
import jwt
import base64
import time
import os


def callback(ch, method, properties, body):
    """
    This function will be called and executed every time that S3I Broker delivers messages to the consuming application (HMI). \n
    The subject and text included in the user message will be parsed and printed. \n
    Attachments will be decoded and stored in folder ../src/demo_received_data.

    Args:
        ch: channel used during consuming, which binds producer and consumer \n
        method: „method“ contains meta information regarding message delivery: It provides delivery flags, redelivery flags, and a routing key that can be used to acknowledge the message. \n
        properties: properties of queue and exchange which are used during consuming \n
        body: content of message 
    """

    body_str = body.decode('utf8').replace("'", '"')  # convert bytes to str
    uMsg = UserMessage(msg_blob=body_str)
    uMsg.convertPgpToMsg()
    # body_json = json.loads(body_str)  # convert str to dict
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S",
                                   time.localtime()) + "]: A new message has been received")
    print("[S3I]: Subject of the message: " + uMsg.msg["subject"])
    print("[S3I]: Text of the message:  " + uMsg.msg["text"])

    attachments_list = list()
    attachments_list = uMsg.msg["attachments"]

    """
    store the attachment file in specified path
    """
    for i in range(len(attachments_list)):
        #cwd = os.getcwd()
        cwd = os.path.dirname(__file__)
        path = os.path.join(cwd, "demo_received_data",
                            attachments_list[i]["filename"])
        file = open(path, 'wb')
        decode = base64.b64decode(attachments_list[i]["data"])
        file.write(decode)
        print("[S3I]: Attachment " + uMsg.msg["attachments"]
              [i]["filename"] + " of the message is stored in " + path)
        file.close()


if __name__ == "__main__":

    """
    Main function for demo 1 receiver HMI: start a listener and wait for the message. \n

    """
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                   ) + "]: DEMO 1, HMI of forest expert is started. Please log in. (Sachverstaendiger)")
    username = input('[S3I]: Please enter the username: \n')
    password = input('[S3I]: Please enter the password: \n')

    '''
    grant type as password is a simplified way to get a JWT. In a subsequent version, grant type as authorization code flow will be implemented.
    '''
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                   ) + "]: Username and password are sent to S3I Identity Provider.")
    s3i_identity_provider = IdentityProvider(grant_type='password', identity_provider_url="https://idp.s3i.vswf.dev/", realm='KWH',
                                             client_id="s3i:6f58e045-fd30-496d-b519-a0b966f1ab01", client_secret="475431fd-2c6d-4cae-bdfa-87226fff0cef", username=username, password=password)
    access_token = s3i_identity_provider.get_token(TokenType.ACCESS_TOKEN)

    '''
    decode the access token 
    verify = False means read the claimset of a jwt without performing validation of the signature or any of the registered claim names
    '''
    parsed_username = jwt.decode(access_token, verify=False)[
        "preferred_username"]

    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S",
                                   time.localtime()) + "]: Token received, " + parsed_username + " logged in.")

    '''
    authentication with JWT in S3I Directory ...
    '''
    s3i_directory = Directory(
        s3i_dir_url="https://dir.s3i.vswf.dev/api/2/", token=access_token)

    '''
    endpoint of schmitz's HMI is known
    '''
    hmi_schmitz_endpoint = "s3ibs://s3i:6f58e045-fd30-496d-b519-a0b966f1ab01"

    '''
    authentication and authorization with access token (JWT) at S3I Broker 
    '''

    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                   ) + "]: Authentication with access token (JWT) at S3I Broker")

    s3i_broker = Broker(
        auth_form='Username/Password', username=" ", password=access_token, host="rabbitmq.s3i.vswf.dev")
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                   ) + "]: Callback function is started, now waiting for the message")

    '''
    start the receive function. The function has a loop which contains a callback function
    if a message is sent to the endpoint of schmitz's HMI, the callback function will be called once 
    '''
    s3i_broker.receive(hmi_schmitz_endpoint, callback)
