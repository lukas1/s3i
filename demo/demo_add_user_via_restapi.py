from s3i import IdentityProvider, Config, Directory, TokenType
import json


if __name__ == "__main__":
    print("************************************************************")
    print("**Welcome to demo: add user via S3I config rest api*********")
    print("************************************************************")
    """query access token of user which exists in S3I. """
    username = input(
        "[S3I]: Please enter username which exists in S3I(KWH-Team): ")
    password = input(
        "[S3I]: Please enter password paired with the above entered username(Password of KWH-Team): ")
    idp_token = IdentityProvider(grant_type="password", client_id="admin-cli",
                                 client_secret="", realm="KWH", identity_provider_url="https://idp.s3i.vswf.dev/",
                                 username=username, password=password)
    token = idp_token.get_token(TokenType.ACCESS_TOKEN)
    print("[S3I]: token obtained")

    """username/password select for creation new user in S3I"""
    create_username = input(
        "[S3I]: Please enter the selected username for new user: ")
    create_password = input(
        "[S3I]: Please enter the selected password for new user: ")

    """config instance"""
    s3i_config = Config(server_url="https://config.s3i.vswf.dev/", token=token)

    """step 1: create person in S3I. Access token is not needed for creation of user"""
    create_person_res = s3i_config.create_person(
        username=create_username, password=create_password)
    print(create_person_res.json())
    dt_person_client_id = create_person_res.json(
    )["thingIdentity"][0]["identifier"]
    dt_person_client_secret = create_person_res.json()[
        "thingIdentity"][0]["secret"]
    dt_person_uuid = create_person_res.json()["personIdentity"]["identifier"]

    """step 2: log in with created user + PW in order to obtain Access Token from S3I IdP"""
    s3i_idp = IdentityProvider(grant_type="password", client_id="admin-cli",
                               client_secret="",
                               realm="KWH", identity_provider_url="https://idp.s3i.vswf.dev/",
                               username=create_username,
                               password=create_password)
    access_token = idp_token.get_token(TokenType.ACCESS_TOKEN)

    """config instance with access token entered"""
    s3i_new_user_config = Config(
        server_url="https://config.s3i.vswf.dev/", token=access_token)

    """step 3: create thing (in this demo, an HMI of new user) in S3I Dir"""
    create_thing_res = s3i_new_user_config.create_thing()
    dt_thing_client_id = create_thing_res.json()["identifier"]
    print(create_thing_res.json())

    """step 4: change thing in S3I Dir"""
    s3i_dir = Directory(
        s3i_dir_url="https://dir.s3i.vswf.dev/api/2/", token=access_token)

    """step 4.1: change thing for the new hmi (add name, type...)"""
    hmi_body = s3i_dir.queryThingIDBased(dt_thing_client_id).json()
    hmi_body["attributes"]["name"] = "HMI Of {}".format(create_username)
    hmi_body["attributes"]["type"] = "hmi"
    res = s3i_dir.updateThingIDBased(dt_thing_client_id, hmi_body)

    """step 4.2: change thing for dt of the neu person (add defaultHMI)"""
    dt_person_body = s3i_dir.queryThingIDBased(dt_person_client_id).json()
    dt_person_body["attributes"]["defaultHMI"] = dt_thing_client_id
    res1 = s3i_dir.updateThingIDBased(dt_person_client_id, dt_person_body)
