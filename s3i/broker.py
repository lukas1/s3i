import sys
import os
import json
import ssl
import pika
from pika.credentials import ExternalCredentials


class Broker:
    """Class Broker contains functions to connect to S3I Broker, and send and receive messages"""

    def __init__(self, auth_form,  content_type="application/json", username=None, password=None, x509_path=None, host="localhost"):
        """Constructor

        :param auth_form: method of authentication at S3I Broker. It can be either X.509 or Username/Password. To authenticate with an access token, using Username/Password.
        :type auth_form: str
        :param content_type: defines what type of data is send to the S3I Broker.
        :type content_type: str
        :param username: name of user which was registered in S3I Broker. If an access token is used to authenticate, username field can be ignored.
        :type username: str
        :param password: password of user which was registered in S3I Broker. If an access token is used to autenticate, password field must be filled with access token.
        :type password: str
        :param x509_path: path of X.509 certificate. If X.509 is chosen as auth_form, x509_path field must be filled
        :type x509_path: str
        :param host: host uri of S3I Broker
        :type host: str
        """
        self.__auth_form = auth_form
        self.__username = username
        self.__password = password
        self.__content_type = content_type
        self.__x509_path = x509_path
        self.__host = host

    @property
    def auth_form(self):
        """

        """
        return self.__auth_form

    @auth_form.setter
    def auth_form(self, value):
        self.__auth_form = value

    @property
    def username(self):
        """
        """
        return self.__username

    @username.setter
    def username(self, value):
        self.__username = value

    @property
    def password(self):
        """
        """
        return self.__password

    @password.setter
    def password(self, value):
        self.__password = value

    @property
    def x509_path(self):
        """
        """
        return self.__x509_path

    @x509_path.setter
    def x509_path(self, value):
        self.__x509_path = value

    @property
    def host(self):
        """Url of S3I Broker
        """
        return self.__host

    @host.setter
    def host(self, value):
        self.__host = value

    @property
    def content_type(self):
        """type of data that is sent to the S3I Broker
        """
        return self.__content_type

    @content_type.setter
    def content_type(self, value):
        self.__content_type = value

    def build_conn_par(self):
        """Authentication at S3I Broker has two different methods
            X.509: Authentication with X.509 certificate which includes the user information or \n
            Username/Password: the corresponding user must have been registered in S3I Broker. To authenticate with an access token, using Username/Password \n
        :return: connection parameters for the S3I Broker; used in function buildConn(self, connpara)
        :rtype: pika connectionParameters, refer to ConnectionParameters_
        """
        auth_list = ["X509", "Username/Password"]
        try:
            auth_list.index(self.auth_form)
        except ValueError as e:
            sys.exit("[x] invalid authentication form, please check")
        else:
            if self.auth_form == "Username/Password":
                credentials = pika.PlainCredentials(
                    username=self.username, password=self.password)
                conn_par = pika.ConnectionParameters(
                    host=self.host, virtual_host='s3i', credentials=credentials)
                return conn_par
            elif self.auth_form == "X509":
                ca_cer_path = os.path.join(
                    self.x509_path, "ca_certificate.pem")
                context = ssl.create_default_context(cafile=ca_cer_path)
                client_cer_path = os.path.join(
                    self.x509_path, "client_certificate.pem")
                client_key_path = os.path.join(
                    self.x509_path, "client_key.pem")
                context.load_cert_chain(client_cer_path, client_key_path)
                ssl_options = pika.SSLOptions(context, self.host)
                conn_par = pika.ConnectionParameters(port=5671, host=self.host, ssl_options=ssl_options,
                                                     virtual_host='s3i', credentials=ExternalCredentials())
                return conn_par

    @staticmethod
    def build_conn(conn_par):
        """Build a pika connection with S3I Broker.

        :param conn_par: connection parameter obtained from brokerAuthentication()
        :type conn_par: connectionParameters
        :return: a connection to the S3I Broker
        :rtype: BlockingConnection, refer to BlockingConnection_
        """
        return pika.BlockingConnection(conn_par)

    @staticmethod
    def build_channel(connection):
        """Build a pika channel in S3I Broker

        :param connection: a blocking connection obtained from buildConn()
        :type connection: BlockingConnection
        :return: a channel to the S3I Broker used to exchange messages
        :rtype: BlockingChannel, refer to BlockingChannel_
        """
        channel = connection.channel()
        return channel

    def receive(self, queue, callback):
        """Wait for S3I-B messages (user message, service request or service reply) and receive every incoming message on the specific queue.

        :param queue: queue which starts a listener in order to receive a single message
        :type queue: str
        :param callback: on_message_callback function
        :type callback: on_message_callback function(ch, method, properties, body) refers to Channel_
        """
        conn = self.build_conn(self.build_conn_par())
        channel = self.build_channel(conn)
        channel.basic_consume(
            queue=queue, on_message_callback=callback, auto_ack=True)
        """consume function refers to Channel_"""
        channel.start_consuming()

    def receive_once(self, queue, callback):
        """Receive one S3I-B message and do not wait for more messages.

        :param queue: queue which starts a listener in order to receive a single message 
        :type queue: str
        :param callback: callback function
        :type callback: callback function(ch, method, properties, body) refers to Channel_
        """
        conn = self.build_conn(self.build_conn_par())
        channel = self.build_channel(conn)
        # channel.basic_get(
        #    queue=queue, callbacks=callback, auto_ack=True)
        channel.basic_get(queue, callback)

    def send(self, receiver_endpoints, msg):
        """Send an S3I-B message

        :param receiver_endpoints: endpoint of the receiver
        :type receiver_endpoints: list of str
        """
        conn = self.build_conn(self.build_conn_par())
        channel = self.build_channel(conn)

        try:
            for i in range(len(receiver_endpoints)):
                channel.basic_publish(exchange="demo.direct", routing_key=receiver_endpoints[i],
                                      properties=pika.BasicProperties(
                                          content_type=self.content_type, delivery_mode=2),
                                      body=msg, mandatory=True)
                """publish function refers to Channel_"""
        except pika.exceptions.UnroutableError as e:
            message = {"error": "{0}".format(  # TODO: add message id
                str(e))}
            sys.exit(message)
        conn.close()
