import pgpy
import os
from datetime import timedelta
from pgpy.constants import PubKeyAlgorithm, KeyFlags, HashAlgorithm, SymmetricKeyAlgorithm, CompressionAlgorithm


class Key:
    """PGP Key as described in RFC 4880"""

    def __init__(self, path_demo_dir=None, filename=None, key_str=None, size=512):
        """This function intializes a PGP key. There are several possibilities to do it: You can specify a path or a key in string format to load an existing key. If neither a path nor a key string is given, a new key is generated.

        :param path_demo_dir: Path to the asc file (default: None)
        :type path_demo_dir: str
        :param filename: name os the asc file storing the PGP key
        :type filename: str
        :param key_str: PGP key in string format (default: None)
        :type key_str: str
        :param size: size of the key if an new key is generated (default: 512)
        :type size: int
        """
        if isinstance(path_demo_dir, str):
            self.loadFromFile(path_demo_dir, filename)
        elif isinstance(key_str, str):
            key_list = pgpy.PGPKey.from_blob(
                key_str.replace("\\n", "\n").strip('"'))
            self.key = key_list[0]
        else:
            self.key = pgpy.PGPKey.new(PubKeyAlgorithm.RSAEncryptOrSign, size)

    def generateKey(self, userID, comment="", email=""):
        self.uid = pgpy.PGPUID.new(userID, comment, email)
        self.key.add_uid(self.uid, usage={KeyFlags.Sign, KeyFlags.EncryptCommunications, KeyFlags.EncryptStorage},
                         hashes=[HashAlgorithm.SHA256, HashAlgorithm.SHA384,
                                 HashAlgorithm.SHA512, HashAlgorithm.SHA224],
                         ciphers=[SymmetricKeyAlgorithm.AES256,
                                  SymmetricKeyAlgorithm.AES192, SymmetricKeyAlgorithm.AES128],
                         compression=[CompressionAlgorithm.ZLIB, CompressionAlgorithm.BZ2, CompressionAlgorithm.ZIP, CompressionAlgorithm.Uncompressed])
        return self.key

    def addKeyExpiration(self, expDays):
        self.key.add_uid(self.uid, key_expires=timedelta(days=expDays))

    def exportsecKeyToFile(self, filename="secKey.asc"):
        save_path = 'key'
        fp = os.path.dirname(os.path.abspath(__file__))
        path = os.path.join(fp, "..", save_path, filename)
        f = open(path, "w")
        keystr = str(self.key)
        f.write(keystr)
        f.close()

    def loadFromFile(self, path_demo_dir, filename):
        """This function loads a PGP key that is given as an asc file from a subdirectory.

        :param path_demo_dir: Path to the asc file (default: None)
        :type path_demo_dir: str
        :param filename: name os the asc file storing the PGP key
        :type filename: str
        """
        save_path = 'key'
        path = os.path.join(path_demo_dir, save_path, filename)
        self.key, _ = pgpy.PGPKey.from_file(path)
