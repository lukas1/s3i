import requests
import json


class Config:
    """class Config contains function to create and delete persons and things"""

    def __init__(self, server_url, token):
        """
        Constructor
        :param server_url: server url of REST API
        :type server_url: str
        :param token: access token of requester (not needed for creating new person)
        :type token: str
        """
        self.__server_url = server_url
        self.__token = token
        """
        Headers for HTTP Requester against S3I Config REST API
        """
        self.__headers = {"Content-Type": "application/json",
                          "Authorization": "Bearer {}".format(self.token)}

    @property
    def server_url(self):
        return self.__server_url

    @server_url.setter
    def server_url(self, value):
        self.__server_url = value

    @property
    def token(self):
        return self.__token

    @token.setter
    def token(self, value):
        self.__token = value

    @property
    def headers(self):
        return self.__headers

    @headers.setter
    def headers(self, value):
        self.__headers = value

    def create_person(self, username, password):
        """
        create person identity and corresponding thing (including policy) in S3I Identity Provider and Directory
        :param username: selected new username
        :type username: str
        :param password: selected password for new user
        :type password: str
        :return: HTTP Response, 201 if OK
        """
        url = self.server_url + "/persons/"
        body = dict()
        body["password"] = password
        body["username"] = username
        return requests.post(url=url, headers=self.headers, data=json.dumps(body))

    def delete_person(self, username):
        """
        delete person identity and all its owned things from S3I Identity Provider, Directory and Broker
        :param username: username of person
        :type username: str
        :return: HTTP Response, 204 if OK
        """
        url = self.server_url + "/persons/{}".format(username)
        return requests.delete(url=url, headers=self.headers)

    def create_thing(self):
        """
        create new thing in S3I, including identity, thing entry and policy in directory, and Broker queue and binding
        :return: HTTP Response, 201 if OK
        """
        url = self.server_url + "/things/"
        return requests.post(url=url, headers=self.headers, data=None)

    def delete_thing(self, thing_id):
        """
        delete identity, directory entry and broker configuration of a thing from S3I IdP, Dir and Broker
        :param thing_id: id of thing
        :type thing_id: str
        :return: HTTP Response, 204 if OK
        """
        url = self.server_url + "/things/{}".format(thing_id)
        return requests.delete(url=url, headers=self.headers)
