import requests
import json


class Directory:
    """Class Directory contains functions to query things in S3I Directory """

    def __init__(self, s3i_dir_url, token):
        """
        Constructor

        :param s3i_dir_url: url of S3I Directory
        :type s3i_dir_url: str
        :param token: id token obtained from S3I IdP
        :type token: str
        """

        self.__s3i_dir_url = s3i_dir_url
        self.__token = token
        """headers to bearer authentication at S3I Directory"""
        self.__s3i_dir_headers = {'Content-Type': 'application/json',
                                  'Authorization': 'Bearer ' + self.token}

    @property
    def s3i_dir_url(self):
        """Url of S3I Directory
        """
        return self.__s3i_dir_url

    @s3i_dir_url.setter
    def s3i_dir_url(self, value):
        self.__s3i_dir_url = value

    @property
    def token(self):
        """ID Token from user which is sent with S3I Directory request
        """
        return self.__token

    @token.setter
    def token(self, value):
        self.__token = value

    @property
    def s3i_dir_headers(self):
        """Header which is sent with S3I Directory request
        """
        return self.__s3i_dir_headers

    @s3i_dir_headers.setter
    def s3i_dir_headers(self, value):
        self.__s3i_dir_headers = value

    def updateThingIDBased(self, thingID, payload):
        """
        Update a thing's description using its ID

        :param thingID: id of the thing
        :type thingID: str
        :param payload: thing description to update in Dir
        :type payload: dict
        :return: requests.Response() see: ResponseObject_
        """
        response = requests.put(url=self.s3i_dir_url + "things/{}".format(thingID),
                                data=json.dumps(payload), headers=self.s3i_dir_headers)
        return response

    def queryThingIDBased(self, thingID):
        """Query a thing's description using its ID

        :param thingID: id of the thing
        :type thingID: str
        :return: response of http requests
        :rtype: requests.Response() see: ResponseObject_
        """
        response = requests.get(
            url=self.s3i_dir_url + "things/" + thingID, headers=self.s3i_dir_headers)
        return response

    def queryAttributeBased(self, key, value):
        """Query a thing's description based on one of its attribute's value

        :param key: fully qualified name of the relevant attribute, e.g., "thingStructure/links/target/values/value"
        :type key: str
        :param value: searched value of this attribute
        :type value: str
        :return: response of http requests
        :rtype: requests.Response() see: ResponseObject_
        """
        response = requests.get(
            url=self.s3i_dir_url +
            'search/things?filter=eq(' + 'attributes/' +
            key + ',"' + value + '")',
            headers=self.s3i_dir_headers)
        return response

    def searchAll(self):
        """List all thing descriptions in S3I Directory

        :return: response of http requests
        :rtype: requests.Response() see: ResponseObject_
        """
        response = requests.get(url=self.s3i_dir_url +
                                "search/things", headers=self.s3i_dir_headers)
        return response

    def getPublicKey(self, thingID):  # TODO thingIDs is list
        if isinstance(thingID, str):
            response = requests.get(url=self.s3i_dir_url+"things/"+thingID +
                                    "/attributes/publicKey", headers=self.s3i_dir_headers)
            return response.text
        if isinstance(thingID, list):
            keys = list()
            for id in thingID:
                response = requests.get(url=self.s3i_dir_url+"things/"+id +
                                        "/attributes/publicKey", headers=self.s3i_dir_headers)
                keys.append(response.text)
            return keys
        else:
            print(
                "[S3I]: Try to get public keys from thing ids but thing ids are neither strings nor lists.")
            return
