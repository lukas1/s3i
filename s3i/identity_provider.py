import requests
import sys
import json
from enum import Enum
import time
import threading


class TokenType(Enum):
    """Enum TokenType covers all available tokens specified in openID connect"""

    #COMPLETE_BUNDLE = 1
    ID_TOKEN = 2
    ACCESS_TOKEN = 3
    REFRESH_TOKEN = 4


class IdentityProvider:
    """ Class IdentityProvider contains functions to communicate with S3I Identity Provider """

    def __init__(self, grant_type, client_id, client_secret, realm, identity_provider_url, username=None, password=None):
        """
        Constructor

        :param grant_type: grant type to obtain JWT. Here only the types "password" and "client_credentials" are implemented. In subsequent version authorization_code will be implemented 
        :type grant_type: str
        :param client_id: id of registered client in S3I Identity Provider
        :type client_id: str 
        :param client_secret: credential of registered client in S3I Identity Provider which must corredspond to client id 
        :type client_secret: str 
        :param realm: registered realm in S3I Identity Provider 
        :type realm: str
        :param identity_provider_url: url of S3I Identity Provider 
        :type identity_provider_url: str 
        :param username: username of registered user in S3I Identity Provider. If grant type is set up as password, this field must be filled 
        :type username: str 
        :param password: password of registered user in S3I Identity Provider. If grant type is set up as password, this field must be filled 
        :type password: str

        """
        self._grant_type = grant_type
        self._client_id = client_id
        self._client_secret = client_secret
        self._realm = realm
        self._username = username
        self._password = password
        self._identity_provider_url = identity_provider_url
        self._identity_provider_get_token = self.identity_provider_url + \
            "auth/realms/" + self.realm + "/protocol/openid-connect/token"
        self._identity_provider_header = {
            "Content-Type": "application/x-www-form-urlencoded"}
        self._token_bundle = None
        self._token_inspector_run = False
        self._last_login = 0

    @property
    def identity_provider_url(self):
        """Url of S3I Identity Provider
        """
        return self._identity_provider_url

    @property
    def identity_provider_get_token(self):
        """Url to obtain a token from the S3I Identity Provider
        """
        return self._identity_provider_get_token

    @property
    def identity_provider_header(self):
        """Header which is sent to the S3I Identity Provider via HTTP
        """
        return self._identity_provider_header

    @property
    def grand_type(self):
        """OAuth grant type which is used
        """
        return self._grant_type

    @property
    def client_id(self):
        """
        """
        return self._client_id

    @property
    def client_secret(self):
        """
        """
        return self._client_secret

    @property
    def realm(self):
        """
        """
        return self._realm

    @property
    def username(self):
        """
        """
        return self._username

    @property
    def password(self):
        """
        """
        return self._password

    def stop_run_forever(self):
        """ Stops the run forever loop when the next iteration happens """
        self._token_inspector_run = False

    def get_token(self, token_type, request_new=False):
        """ Returns a token from the S3I Identity Provider, request a new one if there is no valid token avilable
            Works only if the identity provider is NOT in the run_forever loop
            :param token_type: type of token, see enum TokenType
            :type token_type: enum
            :param request_new: request a new token in any case
            :type request_new: bool 
            :return: token
            :rtype: str
        """
        if self._token_inspector_run:
            return None
        if request_new:
            self._authenticate()
        else:
            if self._token_bundle == None:
                self._authenticate()
            if self._time_until_token_valid() <= 0:
                self._authenticate()
        # all tokens are valid
        if token_type == TokenType.ACCESS_TOKEN:
            return self._token_bundle["access_token"]
        elif token_type == TokenType.ID_TOKEN:
            return self._token_bundle["id_token"]
        elif token_type == TokenType.REFRESH_TOKEN:
            return self._token_bundle["refresh_token"]
        return None

    def run_forever(self, token_type, on_new_token, sleep_interval=5):
        """ Requests tokens from the S³I Identity Provider and refreshs them if they reach their timeout
            :param token_type: type of token, see enum TokenType (REFRESH_TOKEN is no valid param)
            :type token_type: enum
            :param on_new_token: callback if a new token is available
            :type on_new_token: callback
        """
        if token_type == TokenType.REFRESH_TOKEN:
            return None
        on_new_token(self.get_token(token_type, request_new=True))
        self._token_inspector_run = True
        threading._start_new_thread(
            self._run_forever_loop, (token_type, on_new_token, sleep_interval))

    def _run_forever_loop(self, token_type, on_new_token, slep_interval):
        while True and self._token_inspector_run:
            while True:
                if not self._token_inspector_run:
                    return None  # exit
                if self._time_until_token_valid() > slep_interval * 2:
                    time.sleep(slep_interval)
                else:
                    break
            # its time to refresh the tokens
            self._refresh_token(self._token_bundle["refresh_token"])
            if token_type == TokenType.ACCESS_TOKEN:
                on_new_token(self._token_bundle["access_token"])
            elif token_type == TokenType.ID_TOKEN:
                on_new_token(self._token_bundle["id_token"])

    def _authenticate(self):
        """ Request the token-bundle from the S³I Identity Provider """
        self._last_login = time.time()
        body = dict()
        body["grant_type"] = self._grant_type
        body["client_id"] = self._client_id
        body["client_secret"] = self._client_secret
        if self.grand_type == "password":
            body["username"] = self._username
            body["password"] = self._password
        body["scope"] = "openid"
        response = requests.post(url=self._identity_provider_get_token,
                                 data=body, headers=self._identity_provider_header)
        if response.status_code != 200:
            sys.exit("[S3I]: Authentication failed, status-code: " +
                     str(response.status_code))
        else:
            self._token_bundle = response.json()

    def _refresh_token(self, token):
        """ Refreshs the given token
            :param token: type of token, see enum TokenType
            :type token: str
        """
        self._last_login = time.time()
        body = dict()
        body["grant_type"] = "refresh_token"
        body["client_id"] = self._client_id
        body["client_secret"] = self._client_secret
        body["refresh_token"] = token
        body["scope"] = "openid"
        response = requests.post(url=self._identity_provider_get_token,
                                 data=body, headers=self._identity_provider_header)
        if response.status_code != 200:
            sys.exit("[S3I]: Refresh token failed, status-code: " +
                     str(response.status_code))
        else:
            self._token_bundle = response.json()

    def _time_until_token_valid(self):
        """ Returns the time until the token expires """
        time_token_valid = self._token_bundle["expires_in"]
        time_since_login = time.time() - self._last_login
        return time_token_valid - time_since_login
