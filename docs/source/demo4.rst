*******************
Demo 4 - Encryption
*******************

PGP-Message exchange between persons
====================================

Similar to the sceanrio in demo 1, a forest owner (sender) sends an S3I-B user message to a forest expert (receiver). In this demo the message will be encrypted and signed using PGP as described in RFC4880 or in a short version in this :ref:`documentation <pgpRef>`. The public keys are retrieved from the directory. The private keys are stored in the *key* folder. Run the following Python scripts to replay this demo:

1.	demo4_receiver_hmi.py simulates the receiver’s human machine interface (HMI), e.g., an app. It starts a listener waiting for messages from the sender until it is stopped manually. 
   •	Enter username/password for forest expert
2.	demo4_sender_hmi.py simulates the sender’s HMI and sends encrypted and signed messages to the receiver 
   •	Enter username/password for forest owner
   •	Enter subject and text 
   •  Optional: Enter the file names of attachments which are located in the folder demo_send_data (space seperated list)   
   

.. toctree::
   :maxdepth: 2
   :titlesonly:
   :caption: Contents:

.. automodule:: demo.demo4_receiver_hmi
    :members:
   
.. automodule:: demo.demo4_sender_hmi
    :members:
