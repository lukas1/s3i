*************
Demo REST API
*************

Demo S3I configuration REST APIs: create person with HMI in S3I via S3I REST APIs
=================================================================================

In this demo, we demonstrate how to configure S3I via its REST APIs in order to create a person and its HMI. When running the Python script demo_add_user_via_restapi.py, enter firstly username and password of a user which exists in S3I. Then select a username and password for the new person. The process within this script consists of four steps: 

Step 1: create a person using S3I Config REST API
	• this process automatically integrates the following actions:
	• creates a person identity in S3I IdentityProvider
	• creates a thing identity for this person's own Digital Twin (DT) in S3I IdentityProvider
	• creates a policy for this DT in S3I Directory
	• creates an entry for this DT in S3I Directory

Step 2: log in with newly created credentials (username + password)
	• query an access token (JWT) from S3I IdentityProvider

Step 3: create a new thing (in this demo, an HMI) using S3I Config REST API
	• this process automatically integrates the following actions:
	• creates a thing identity for the new HMI in S3I IdentityProvider
	• creates a policy for this HMI in S3I Directory
	• creates an entry for this HMI in S3I Directory
	• creates a queue and binding in S3I Broker

Step 4: update thing description for HMI and person's DT in S3I Directory
	• e.g., set name, type for HMI, defaultHMI for person's DT etc. 

More details can be found in the Python script.

.. toctree::
   :maxdepth: 2
   :titlesonly:
   :caption: Contents:

.. automodule:: demo.demo_add_user_via_restapi
    :members:
