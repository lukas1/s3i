# Roadmap

## February 2020:
* Message encryption with Json Web Encryption
* REST API for the configuration of the Directory, Identity Provider and Broker
* Configuration Class in S3I Python Package to add new things to the S3I and thereupon configure the Directory, Identity Provider and Broker, respectively
* Integrate those things into the S3I that were shown at the presentation of the Smart Forest Lab on May 8, 2019. 
* Provide a Cloud Platform as another component of the S3I (beside the Directory, the Identity Provider and the Broker) where cloud-based Digital Twins are located. (An Edge-based Digital Twin will be introduced in the KWH4.0)

## Following:
* REST API for the Broker providing endpoints to use Broker communication functions like send and receive
* Introduce events with which users and applications can be notified by the S3I.
* Web interface for the convenient configuration, management and use of the S3I