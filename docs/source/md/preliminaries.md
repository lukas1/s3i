# Preliminaries

## Requirements
* Python 3+
* recommended: virtualenv and optionally, but also recommended virtualenvwrapper
To install virtualenv and virtualenvwrapper, just execute (after installing Python):
```
pip install virtualenv
pip install virtualenvwrapper-win
```

More Information on how to use virtualenv can be found here: [The Hitchhiker's Guide to Python](http://docs.python-guide.org/en/latest/dev/virtualenvs/)

## Preliminaries
We recommend creating a virtual environment for the project (`mkvirtualenv projectname`). After creating the environment, activate it (`workon projectname`). Navigate to the root folder containing the `requirements.txt` and install the required modules for the demo with (`pip install -r requirements.txt`).


## Folders
Two Folders located in folder ../demo/ are neccessary to replay the demos: 

```
demo_send_data
demo_received_data
```
These folders contain demo data. More information about sent and received example files can be found in the [KWH4.0-Standpunkt](https://www.kwh40.de/wp-content/uploads/2019/12/KWH40-Standpunkt-S3I-v1.0.pdf) .