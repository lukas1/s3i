# REST APIs
#### Communication via REST APIs

S3I's services can all be accessed using REST APIs. 

### S3I IdentityProvider
The S3I IdentityProvider's REST API is available at
```
"https://idp.s3i.vswf.dev/auth/realms/KWH/protocol/openid-connect/"
```
To get S3I IdentityProvider's public key (to validate the access token (JWT) it provides), you can use (without providing an authorisation header) a HTTP GET to:
```
"https://idp.s3i.vswf.dev/auth/realms/KWH/protocol/openid-connect/certs"
``` 
To get a JWT authorization token, this API can be used with a HTTP POST:
```
"https://idp.s3i.vswf.dev/auth/realms/KWH/protocol/openid-connect/token"
```
The request must use a body comprising:
```
{
	"grant-type": "password",
	"client-id": <your_client_id>,
	"client-secret": <your_client_secret>,
	"username": <your_username>,
	"password": <your_password>
}
```
When a person wants to get a token, its PersonIdentity (username and password) must be used. Additionally, the credentials of the ThingIdentity (client-id and -secret) of the HMI it uses must also be provided. If no HMI is in use or available, "admin-cli" must be provided as client-id and the client-secret can be ignored.

Besides the body, a header has to be specified:
```
{"Content-Type": "application/x-www-form-urlencoded"}
```

More details of the REST API of S3I IdentityProvider's underlying Keycloak can be found here: [IdP Admin REST API](https://www.keycloak.org/docs-api/5.0/rest-api/index.html#_overview).

### S3I Directory

S3I Directory provides a REST API to query and update things and policies. It is available at:
```
"https://dir.s3i.vswf.dev/api/2/"
```
A comprehensive documentation can be found at [Dir REST API](https://dir.s3i.vswf.dev/apidoc/). Note, however, that only querying and updating existing entries is supported by S3I Directory. Completely new entries are created using S3I Config (see below).

Every REST call must comprise an authorization bearer header with an access token (JWT):
```
{
	"Content-Type": "application/json", 
	"Authorization": "Bearer <access token>"
}
```

Example 1: Find Harvester thing based on its identifier s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63
```
HTTP request: GET
url: "https://dir.s3i.vswf.dev/api/2/things/s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63"
body: empty
result: HTTP response 
status code: 200,
response body: 
{
    "thingId": "s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63",
    "policyId": "s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63",
    "attributes": {
        "ownedBy": "606d8b38-4c3f-46bd-9482-86748e108f32",
        "name": "Harvester",
        "location": {
            "longitude": 10.117,
            "latitude": 20.136
        },
        "publicKey": "...",
        "type": "component",
        "dataModel": "fml40",
        "allEndpoints": [...],
        "thingStructure": {
            "class": "fml40.Harvester",
            "services": [...],
            "links": [...]
        }
    }
}

```
Example 2: Get location of Harvester:
```
HTTP request: GET
url: "https://dir.s3i.vswf.dev/api/2/things/s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63/attributes/location"
body: empty
result: HTTP response 
status code: 200,
response body: 
{
    "latitude": 10.117,
    "longitude": 20.136
}
```
Example 3: Update location of this Harvester:
```
HTTP request: PUT
url: "https://dir.s3i.vswf.dev/api/2/things/s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63/attributes/location"
body: 
{
    "latitude": 21.221,
    "longitude": 10.1852
}
result: HTTP response
status code: 201
```

### S3I Broker

We are currently developing a REST API for the S3I Broker for a convenience access. It will allow to send and receive S3I-B messages without using the AMQP protocol.

### S3I Config

For a coordinated creation and deletion of persons and things within all of S3I's services, an additional S3I Config REST API is provided. It is available at:
```
"https://config.s3i.vswf.dev/"
```
To use this REST API, an authorization bearer header must be provided for every request:
```
{
	"Content-Type": "application/json", 
	"Authorization": "Bearer <access token>"
}
```
To know more about how to query an access token, see the REST API of S3I IdentityProvider. 

#### Add person 
New person can be created in S3I by existing person (with providing its access token in an authorization bearer header) using a HTTP POST request to the REST API which is available at:
```
"https://config.s3i.vswf.dev/persons/"
```

A body with the new credentials (username and password) must be provided:
```
{
    "username": <your_new_username>,
    "password": <your_new_password>
}
```
The process creates a new person identity with the given credentials. Additionally, a personal Digital Twin is created comprising a thing identity (including credentials) and an entry in the S3I Directory. The relevant identity information is provided in the HTTP response:
```
{
	"personIdentity": {
		"username": <your_new_username>,
		"identifier": <identifier_of_new_user>},
	"thingIdentity": {
		"identifier": <identifier_of_person_dt>,
		"secret": <secret_of_this_dt>}
}
```
#### Delete person
To delete a person identity, the following REST API can be used with a HTTP DELETE request, providing the person's username:
```
"https://config.s3i.vswf.dev/persons/<person_username>"
```
To use this REST API, an access token of this person is needed in the bearer header. 
If the person is successfully deleted, a status code as 204 will be returned as HTTP response, otherwise an error with a corresponding status code. 
#### Add thing
For an existing person identity, a new thing can be created using the following REST API with a HTTP POST request:
```
"https://config.s3i.vswf.dev/things/"
```
An authorization bearer header must be provided in th request as well. After creation the requester will be assigned as the owner of the created thing. The created thing identity (thing identifier and secret) will be returned in HTTP response:
```
{
    "identifier": <identifier_of_new_thing_dt>,
    "secret": <secret_of_this_dt>
}
```
#### Delete thing
A thing can be deleted from S3I using an HTTP DELETE request with the following REST API, providing the thing's identifier:
```
"https://config.s3i.vswf.dev/things/<thing_id>"
```
To use this REST API, an access token of thing's owner must be provided in the header. 
If the thing is successfully removed from S3I, a status code as 204 will be returned in HTTP response, otherwise an error with a corresponding status code.
