# IDs currently used in S3I Directory 
## Person Identities
Waldbesitzer: &emsp;             5051996f-c37a-4a34-947e-127c9d9891e9  
Sachverstaendiger: &emsp;        800ee8e8-4873-4792-9294-c81948710938  
Waldarbeiter Karl: &emsp;        3ed0d26e-5a75-47b9-b934-31a51a3820e5  
Waldarbeiter Walter: &emsp;      0adfa9f5-1e3e-4bd8-886f-9cb010f0c92a  
KWH-Team: &emsp;                 606d8b38-4c3f-46bd-9482-86748e108f32  

## Thing Identities 
DT Waldbesitzer:&emsp;            s3i:e61175c7-93dc-48f3-93f4-10aa186cc5e7  
HMI Waldbesitzer:&emsp;           s3i:2aafd97c-ff05-42b6-8e4d-e492330ec959  
DT Sachverstaendiger:&emsp;       s3i:2a2727eb-3be6-4c53-9300-6269a0969d43  
HMI Sachverstaendiger:&emsp;      s3i:6f58e045-fd30-496d-b519-a0b966f1ab01  
DT Waldarbeiter Karl:&emsp;       s3i:5a256363-4e77-4436-8c14-20b147b38819  
DT Waldarbeiter Walter:&emsp;     s3i:0c253262-428e-44be-a11a-b83566bd1f68  
  
DT Harvester:&emsp;               s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63  
DT Forwarder A:&emsp;             s3i:c81c7f54-46f4-40d6-a6e6-4bd0e8a7f08d  
DT Forwarder B:&emsp;             s3i:f3302964-0110-424f-81b7-dc90127cd745  
DT Lkw:&emsp;                     s3i:9a6c2c61-0a01-4571-b6cf-ef4a7b37ac9c  
DT Radlader:&emsp;                s3i:047b1275-776e-4e1a-8e9c-aa644c2cd463  
  
DT Saegewerk:&emsp;               s3i:2073c475-fee5-463d-bce1-f702bb06f899  
DT Notrufzentrale:&emsp;          s3i:be074842-7d3c-4956-bc9d-c322808c4b65  
  
DT Umweltsensor:&emsp;            s3i:fa74c9ee-059b-430c-b672-59e52ce690f1  
DT Polter:&emsp;                  s3i:b4169079-7451-41de-b266-721fe1f0bda5  
  
DT 3D Dashboard:&emsp;            s3i:6197b70f-226f-4986-bb77-1f8919e89aaf  
DT 2D Dashboard:&emsp;            s3i:f357e49e-ee21-4ab6-bd0f-43cf24e517b4  
  
Service Stand attributes:&emsp;   s3i:ab0717b0-e025-4344-8598-bcca1d3d5d47  
Service Tree Species
 Classification:&emsp;            s3i:897b771b-8a3c-4104-926e-fbdca73ad4a3  
Service Befahrbarkeit:&emsp;      s3i:1367de5a-6b9d-4403-90a7-7ee9eb157b43  
Service Proximity Alert:&emsp;    s3i:c8d7d60d-d682-4798-88a1-0cfc3aec5bc0  
