.. |pgp| image:: fig/PGP.png
.. _license: https://en.wikipedia.org/wiki/Pretty_Good_Privacy#/media/File:PGP_diagram.svg 
.. _copyright: https://creativecommons.org/licenses/by-sa/3.0/
.. _pgpRef: 
***
PGP
***

Pretty-Good-Privacy
===================

|pgp|
Figure 1: PGP encryption and decryption process. From the graphic *How PGP encryption works* made by `xaedes&jfreax&Acdx <license>`_. License: copyright_.

Pretty-Good-Privacy (PGP) is an encryption standard providing cryptographic privacy and authentication for data communication. The Network Working Groups describes the Open PGP Message Format in the RFC 4880. The format uses a combination of symmetric-key and public-key cryptography.

Using symmetric-key cryptography, a message is encrypted and decrypted with the same crypthographic keys. The two keys might have different formats but they are based on the same secret. This secret has to be shared between the involved parties which presumes a secure channel for the exchange. Public-key or asymmetric cryptography avoids the shared secret. Each user provides a key pair consisting of a public and a private key. The keys are generated in such way that encrypting data with one of these keys it can only be decrypted with its matching couterpart. Providing that the private key is always kept private, someone encrypting data with the corresponding public key induces that only the holder of the private key can decrypt the data. This way using someones public key it’s assured that only he can decrypt the message if the affiliation of the private key to the expected receiver is granted. That way, the privacy of a PGP message is assured.

The private key can not only be used for decryption, but also to sign message. Again, only the expected authority of the private key uses the private key so that if something is signed with that key one can be certain that this particular authority signed the message. The signature of a message is a hash which is encrypted with a private key. It gets attached to the actual message. When the receiver decryptes the signature with the respective public key it should match with the hash of the message, which he calculated by himself. If the message gets modified the verification would fail since the signature wasn't created based on the modified message but on the original one. Using signatures like this ensures authenticity and integrity of the message.

Comparing symmetric-key and public-key cryptography the symmetric encryption is more lightwight, meaning it needs less comupter resources and is faster compared to asymmetric encryption. Still, it does not provide message authentication using signatures and is less convenient with the distribution of the keys. PGP combines the advantages of assymetric and symmetric encryption by encrypting the data with a symmetric key, encrypting this key with public-key crypthography and send it together with the data. This way the relatively ressource and time consuming public-key encryption is done only on the symmetric key and the actual data encryption is done in a faster and resource-saving way using a symmetric key. The process is shown in figure 1. If there are several receiver the symmetric key has to be encrypted for each receiver with its respective public key. Also the message could be signed before its encryption which is ignored in the shema.

The RFC standard does not force the concrete encryption algorithms used, but states which algorithms must, should and may be implemented by an PGP application. In the S3I we use the RSA (Rivest–Shamir–Adleman) algorithm with a 2048 bit long key for public-key encryption and an AES (Advanced Encryption Standard) with a 512 bit long key for symmetric encryption. 