************************
Demo 2 - User to service
************************

Calculate stock value via service
=================================

In demo 2, the forest expert uses a service to calculate the stock value for a given surface geometry. For that purpose, his HMI sends an S3I-B service request message and the service replies with an S3I-B service reply message. Run the following Python scripts to replay this demo:

1.	demo2_service.py simulates a service calculating stock values and communicating via S3I-B messages (service request and service reply). It starts a listener waiting for messages until it is stopped manually.
   
2.	demo2_sender_hmi.py simulates an HMI of the forest expert (e.g., an app). It sends an S3I-B service request to the service and then waits for the reply. Subsequently, the reply message with the stock value is received and displayed.
   •	Enter username/password for forest expert

.. toctree::
   :maxdepth: 2
   :titlesonly:
   :caption: Contents:
   
.. automodule:: demo.demo2_sender_hmi
    :members:

.. automodule:: demo.demo2_service
    :members:   
