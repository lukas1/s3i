# Documentation
This package contains everything you need to get started with the S3I. The documentation of the S3I package and its demos can be found [here](https://git.rwth-aachen.de/kwh40/s3i/-/jobs/artifacts/master/file/public/index.html?job=pages).

# Install s3i package
To use the s3i package in your own project you can install it using the latest [wheel](https://git.rwth-aachen.de/kwh40/s3i/-/jobs/artifacts/master/raw/public/s3i-0.4-py3-none-any.whl?job=wheel). 

To install this wheel, go to the respective directory or switch to your designated virtual environment and install the *.whl* file (`python -m pip install 
https://git.rwth-aachen.de/kwh40/s3i/-/jobs/artifacts/master/raw/public/s3i-0.4-py3-none-any.whl?job=wheel`). This will install the s3i package and all the packages it depends on.